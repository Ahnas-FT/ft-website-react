import React from "react";
import Header from "../../Components/Header/Header";
import Banner from "../../Components/Banner/Banner";
import ServiceArea from "../../Components/ServiceArea/ServiceArea";
import Utility from "../../Components/Utility/Utility";
import WhyArea from "../../Components/WhyArea/WhyArea";
import ContactArea from "../../Components/ContactArea/ContactArea";
import SubscribeArea from "../../Components/SubscribeArea/SubscribeArea";
import Footer from "../../Components/Footer/Footer";
import DotsMenu from "../../Components/DotsMenu/DotsMenu";

function Home() {
  return (
    <div>
      <Header />
      <DotsMenu />
      <Banner />
      <ServiceArea />
      <Utility />
      <WhyArea />
      <ContactArea />
      <SubscribeArea />
      <Footer />
    </div>
  );
}

export default Home;
