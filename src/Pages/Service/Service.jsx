import React from 'react'
import Header from "../../Components/Header/Header";
import Footer from "../../Components/Footer/Footer";
import PageTitle from '../../Components/PageTitle/PageTitle';
import ServicePageArea from '../../Components/ServicePageArea/ServicePageArea';

import servicebg from "../../Images/services-banner.jpeg";

function Service() {
  return (
    <div>
      <Header />
      <PageTitle heading="Service" bg={servicebg} />

      <ServicePageArea />

      <Footer />
    </div>
  );
}

export default Service