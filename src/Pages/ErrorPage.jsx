import React from 'react'
import Header from '../Components/Header/Header';

function ErrorPage() {
  return (
    <div>
      <Header />
      <h2 className='text-center'>404-ErrorPage - NOT Found</h2>
    </div>
  );
}

export default ErrorPage;