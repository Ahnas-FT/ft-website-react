import React from 'react'
import Footer from '../../Components/Footer/Footer';
import Header from '../../Components/Header/Header';
import PageTitle from '../../Components/PageTitle/PageTitle';
import LocationContact from '../../Components/LocationContact/LocationContact';

import contactbg from "../../Images/contact-banner.jpeg";

export default function Contact() {
  return (
    <div>
      <Header />
      <PageTitle heading="Contact" bg={contactbg} />
      <LocationContact />
      <Footer />
    </div>
  );
}
