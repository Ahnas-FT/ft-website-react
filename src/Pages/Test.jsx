import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";
import Logo from "../Components/Header/Logo/Logo";

import { Link } from "react-router-dom";
import HeaderExtra from "../Components/Header/HeaderExtra/HeaderExtra";
import NavbarItem from "../Components/Header/NavbarItem/NavbarItem";



export default function Test() {
  const mystyle = {
    height: "45px",
  };

  return (

    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand href="#home">
          <Link to="/">
            <Logo style={mystyle} name="logo-dark pl-3" />
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="place-content-center">
          <Nav className="me-auto">

            <NavbarItem name="Home" url="/" />
            <NavbarItem name="Service" url="/service" />


          </Nav>
        </Navbar.Collapse>

          <HeaderExtra />

      </Container>
    </Navbar>

  );
}
