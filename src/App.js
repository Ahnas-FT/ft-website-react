import './App.css';
import React from "react";

import { BrowserRouter as Router, Switch, Routes, Link, Route } from "react-router-dom";

import Home from './Pages/Home/Home';

import ErrorPage from './Pages/ErrorPage';
import Service from './Pages/Service/Service';
import Contact from './Pages/Contact/Contact';
import Test from './Pages/Test';




function App() {
  return (
    <div className="body-inner">
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/service" element={<Service />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/test" element={<Test />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
