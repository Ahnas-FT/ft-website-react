import React from "react";
import PropTypes from "prop-types";
import bgimgUtility from "./../../Images/8.png";
import UtiltyItem from "../UtilityItem/UtiltyItem";

function Utility() {
  return (
    <div>
      <section
        id="section3"
        className="p-t-100 p-b-100"
        data-bg-parallax={bgimgUtility}
      >
        <div className="container xs-text-center sm-text-center text-light">
          <div className="row">
            <UtiltyItem name="Telecom" icon="carbon:phone-ip" />
            <UtiltyItem name="Finance" icon="map:finance" />
            <UtiltyItem name="Healthcare" icon="cil:hospital" />
            <UtiltyItem name="Education" icon="cil:education" />
            <UtiltyItem
              name="Public Safety"
              icon="ant-design:safety-outlined"
            />
          </div>
        </div>
      </section>
    </div>
  );
}

export default Utility;
