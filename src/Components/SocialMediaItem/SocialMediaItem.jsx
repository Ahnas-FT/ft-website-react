import React from "react";
import { Link } from "react-router-dom";

function SocialMediaItem(props) {
  return (
    <li className="social-linkedin">
      <Link target="blank" to={props.url}>
        <i className={props.icon}></i>
      </Link>
    </li>
  );
}

export default SocialMediaItem;
