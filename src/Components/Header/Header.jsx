import React from "react";
import Logo from "./Logo/Logo";
import HeaderExtra from "./HeaderExtra/HeaderExtra";
// import MenuTrigger from "./MenuTrigger/MenuTrigger";
// import Navbar from "./Navbar/Navbar";

import { Link } from "react-router-dom";

import { Container, Navbar, Nav } from "react-bootstrap";
import NavbarItem from "./NavbarItem/NavbarItem";

function Header() {
  const mystyle = {
    height: "45px",
  };


  return (
    <Navbar bg="light" expand="lg">
      <Container fluid className="z-index-10">
        <Navbar.Brand href="/">
          <Link to="/">
            <Logo style={mystyle} name="logo-dark pl-3" />
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle id="pos-right" aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="place-content-center">
          <Nav className="me-auto">
            <NavbarItem name="Home" url="/" />
            <NavbarItem name="Service" url="/service" />
          </Nav>
        </Navbar.Collapse>

        <HeaderExtra />
      </Container>
    </Navbar>
  );
}

export default Header;
