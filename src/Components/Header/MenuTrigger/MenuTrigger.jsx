import React from "react";

function MenuTrigger() {
  return (
    <div id="mainMenu-trigger">
      <button onClick={()=>alert("hello")} className="lines-button x">
        <span className="lines"></span>
      </button>
    </div>
  );
}

export default MenuTrigger;
