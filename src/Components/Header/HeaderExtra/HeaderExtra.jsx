import React from "react";
import { Link } from "react-router-dom";

function HeaderExtra() {
  return (
    <div className="header-extras">
      <ul>
        <li className="d-none d-xl-block d-lg-block">
          <Link to="/Contact" className="btn btn-rounded btn-light">
            Contact Us
          </Link>
        </li>
      </ul>
    </div>
  );
}

export default HeaderExtra;
