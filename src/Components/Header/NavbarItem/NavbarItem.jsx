import React from "react";
import { Link } from "react-router-dom";

function NavbarItem(props) {
  return (
    <li>
      <Link to={props.url}>
        <span> {props.name}</span>
      </Link>
    </li>
  );
}

export default NavbarItem;
