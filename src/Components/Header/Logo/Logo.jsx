import React from "react";
import imageURL from "../../../Images/logo.png";

function Logo({ children, ...otherprops }) {
  return (
    <img
      className={otherprops.name}
      {...otherprops}
      src={imageURL}
      alt="Logo"
    />
  );
}

export default Logo;
