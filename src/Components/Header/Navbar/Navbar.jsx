import React from "react";
import NavbarItem from "../NavbarItem/NavbarItem";

function Navbar() {
  

  return (

    <div id="mainMenu" className="menu-center light">
      <div className="container">
        <nav>
          <ul>
            <NavbarItem name="Home" url="/" />
            <NavbarItem name="Service" url="/service" />
          </ul>
        </nav>
      </div>
    </div>
  );

}

export default Navbar;
