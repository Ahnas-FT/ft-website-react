import React from "react";
import FooterNav from "./../FooterNav/FooterNav";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <footer id="footer">
      <div className="footer-content">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="widget">
                <div className="widget-title clear-padd">
                  Freston ANALYTICS Private limited
                </div>
                <p className="mb-5">
                  <br />
                  We provide organisations with premium software, web
                  development services and access to a pool of expert developers
                  who are extremely passionate about what they do.
                </p>
                <Link to="/contact" className="btn btn-inverted">
                  Get in Touch
                </Link>
              </div>
            </div>

            <div className="col-lg-6">
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-12">
                  <div className="widget">
                    <div className="widget-title">Discover</div>
                    <ul className="list">
                      <FooterNav url="" name="GDPR" />
                      <FooterNav url="" name="Terms of Use" />
                      <FooterNav url="" name="Cookie Policy" />
                    </ul>
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-12">
                  <div className="widget">
                    <div className="widget-title">Pages</div>
                    <ul className="list">
                      <FooterNav url="" name="Home" />
                      <FooterNav url="" name="About" />
                      <FooterNav url="" name="Service" />
                    </ul>
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-12">
                  <div className="widget">
                    <div className="widget-title">Support</div>
                    <ul className="list">
                      <FooterNav name="Contact" url="/contact" />
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright-content">
        <div className="container">
          <div className="copyright-text text-center">
            &copy;
            {new Date().getFullYear()}
            <Link to="https://frestonanalytics.com/">-Freston Analytics.</Link>
            All Rights Reserved.
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
