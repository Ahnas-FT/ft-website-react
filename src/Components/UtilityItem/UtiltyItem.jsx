import React from "react";
import PropTypes from "prop-types";

function UtiltyItem(props) {
  return (
    <div className="col-lg col-md-6 col-sm-12 col-12 margin-bottom-40">
      <div className="text-center">
        <span className="iconify" data-icon={props.icon}></span>
        <div className="counter">
          <h3>{props.name}</h3>
        </div>
      </div>
    </div>
  );
}

export default UtiltyItem;
