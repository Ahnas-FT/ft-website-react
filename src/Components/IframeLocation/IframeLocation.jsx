import React from "react";

function IframeLocation(props) {
  return (
    <iframe
      src={props.link}
      width="100"
      height="400"
      loading="lazy"
      className="border-0"
    ></iframe>
  );
}

export default IframeLocation;
