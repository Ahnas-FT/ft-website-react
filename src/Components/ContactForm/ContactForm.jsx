import React from 'react'

function ContactForm() {
  return (
    <form id="submit-form" className="" action="" role="form" method="POST">
      <div className="row">
        <div className="form-group col-md-6">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            aria-required="true"
            name="name"
            className="form-control required name"
            placeholder="Enter your Name"
          />
        </div>
        <div className="form-group col-md-6">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            required
            name="email"
            className="form-control"
            placeholder="Enter your Email"
          />
        </div>
      </div>
      <div className="row">
        <div className="col-md-6 form-group">
          <label className="upper" htmlFor="phone">
            Phone
          </label>
          <input
            type="text"
            className="form-control"
            name="phone"
            placeholder="Enter phone"
          />
        </div>
        <div className="form-group col-lg-6">
          <label>Services</label>
          <select name="service">
            <option value="">Select service</option>
            <option value="Wordpress">Digital Transformation</option>
            <option value="Software Engineering">Software Engineering</option>
            <option value="Cloud Migration">Cloud Migration</option>
            <option value="Graphic Design">Graphic Design</option>
          </select>
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="message">Message</label>
        <textarea
          type="text"
          name="message"
          rows="8"
          className="form-control required"
          placeholder="Enter your Message"
        ></textarea>
      </div>
      <h6 className="" style={{ color: "red" }} id="response"></h6>
      <div className="form-group">
        <button id="load-sub" className="btn btn-light load-sub" type="submit">
          <i className="fa fa-paper-plane"> </i>&nbsp; Send message
        </button>
        <div className="float-right">
          <div className="loader"></div>
        </div>
      </div>
    </form>
  );
}

export default ContactForm