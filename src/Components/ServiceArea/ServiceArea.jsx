import React from "react";
import ServiceItem from "../ServiceItem/ServiceItem";

function ServiceArea() {
  return (
    <div>
      
      <section
        id="section1"
        className="no-padding equalize"
        data-equalize-item=".text-box"
      >

        <div className="row col-no-margin">
          <ServiceItem
            icon="cil:apps-settings"
            name="Digital Transformation"
            detail="Digital strategy to help unlock potential, innovate and invest
                  your time where it matters."
          />
          <ServiceItem
            icon="eos-icons:software-outlined"
            name="Software Engineering"
            detail="Expert teams to deliver path-breaking business critical
                  software development projects."
          />
          <ServiceItem
            icon="bi:cloud-check"
            name="Cloud Migration"
            detail="Efficiently move processes and data to the cloud, using Azure
                  technology and infrastructure."
          />
          <ServiceItem
            icon="clarity:analytics-outline-badged"
            name="Data Analytics"
            detail="Dig deep into your data to understand and optimise performance
                  drivers and behaviour across your business."
          />

        </div>
      </section>
    </div>
  );
}

export default ServiceArea;
