import React from "react";

function Address(props) {
  return (
    <div className="col-lg-6 m-b-30">
      <address>
        <strong>{props.name}</strong>
        <br />
        {props.lineOne}
        <br />
        {props.lineTwo}
        <br />
        {props.lineThree}
        <br />
      </address>
    </div>
  );
}

export default Address;
