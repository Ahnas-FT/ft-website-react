import React from "react";
import { Link } from "react-router-dom";


function WhyArea() {
  return (
    <section id="section4" className="p-t-150 p-b-100">
      <div className="container">
        <div className="col-lg-12 m-b-100 center">
          <div className="heading-text heading-section text-center">
            <h2>Why Freston Analytics</h2>
            <p>
              We provide organisations with premium software, web development
              services and access to a pool of expert developers who are
              extremely passionate about what they do.
            </p>
            <p></p>
          </div>
        </div>
        <div className="row place-content-center">
          <div className="col-lg-6">
            <div className="icon-box text-center effect border color">
              <div className="icon">
                <Link to="">
                  <span
                    className="iconify"
                    data-icon="fluent:people-team-24-regular"
                  ></span>
                </Link>
              </div>

              <p className="lead">
                Our team will work collaboratively with you from the start to
                understand your requirements and keep the delivery of your
                project agile, accurate and cost-efficient.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default WhyArea;
