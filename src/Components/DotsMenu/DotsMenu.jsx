import React from "react";
import NavbarItem from "../Header/NavbarItem/NavbarItem";

function DotsMenu() {
  return (
    <nav id="dotsMenu">
      <ul>
        <NavbarItem name="Home" url="#home" />
        <NavbarItem name="About" url="#section4" />
        <NavbarItem name="Contact" url="#section7" />
        <NavbarItem name="Subscribe" url="#subscribe" />
      </ul>
    </nav>
  );
}

export default DotsMenu;
