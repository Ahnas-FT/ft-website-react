import React from "react";

import ContactNavItem from "../ContactNavItem/ContactNavItem";
import Address from "../Address/Address";
import IframeLocation from "../IframeLocation/IframeLocation";
import SocialMediaItem from "../SocialMediaItem/SocialMediaItem";
import ContactForm from "../ContactForm/ContactForm";

function LocationContact() {
  return (
    <div className="container mt-5">
      <div className="row">
        <div className="content col-lg-12">
          <div className="tabs">
            <ul className="nav nav-tabs" id="myTab" role="tablist">
              <ContactNavItem name="UK" link="#home" />
              <ContactNavItem name="IN" link="#profile" />
            </ul>

            <div className="tab-content" id="myTabContent">
              <div
                className="tab-pane fade show active"
                id="home"
                role="tabpanel"
                aria-labelledby="home-tab"
              >
                <div className="row">
                  <div className="col-lg-5 ">
                    <Address
                      name="Freston Analytics UK Ltd"
                      lineOne="200 Brook Drive"
                      lineTwo="Green Park, Reading"
                      lineThree="RG2 6UB - United Kingdom"
                    />
                  </div>
                  <div className="col-lg-7">
                    <IframeLocation link="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2524.5959021083445!2d-1.8213724842912367!3d50.74598337951723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48739f2b744d7a7d%3A0xab1251e65965b948!2sEverdene%20House%2C%20Deansleigh%20Rd%2C%20Bournemouth%20BH7%207DU%2C%20UK!5e0!3m2!1sen!2sin!4v1645442140105!5m2!1sen!2sin" />
                  </div>
                </div>
              </div>

              <div
                className="tab-pane fade"
                id="profile"
                role="tabpanel"
                aria-labelledby="profile-tab"
              >
                <div className="row">
                  <div className="col-lg-5 ">
                    <Address
                      name="Freston Analytics IN"
                      lineOne="Thanveer Complex"
                      lineTwo="West Nadakkavu"
                      lineThree="Kerala, India"
                    />
                  </div>
                  <div className="col-lg-7">
                    <IframeLocation link="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.9176695120314!2d75.77436831475342!3d11.26746229198947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba659336af4684b%3A0x7197043f10cd7180!2sThanveer%20Complex!5e0!3m2!1sen!2sin!4v1645442213718!5m2!1sen!2sin" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <h3 className="text-uppercase">Get In Touch</h3>
              <p>
                We provide organisations with premium software, web development
                services and access to a pool of expert developers who are
                extremely passionate about what they do.
              </p>
              <div className="social-icons m-t-30 social-icons-colored">
                <ul>


                  <SocialMediaItem
                    url="https://www.linkedin.com/company/freston-analytics-pvt-ltd"
                    icon="fab fa-linkedin"
                  />
                  <SocialMediaItem
                    url="https://www.youtube.com/company/freston-analytics-pvt-ltd"
                    icon="fab fa-youtube"
                  />
                </ul>
              </div>
            </div>
            <div className="col-lg-6">

              <ContactForm />

            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default LocationContact;
