import React from "react";

import { Link } from "react-router-dom";

function ContactNavItem(props) {
  return (
    <li className="nav-item">
      <Link
        className="nav-link"
        id="home-tab"
        data-toggle="tab"
        to={props.link}
        role="tab"
        aria-controls="home"
        aria-selected="true"
      >
        <strong>{props.name}</strong>
      </Link>
    </li>
  );
}

export default ContactNavItem;
