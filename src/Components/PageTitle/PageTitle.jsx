import React from "react";

import { Link } from "react-router-dom";


function PageTitle(props) {
  return (
    <section id="page-title" className="text-light" data-bg-parallax={props.bg}>
      <div className="container">
        <div className="page-title">
          <h1>{props.heading}</h1>
        </div>
        <div className="breadcrumb">
          <ul>
            <li>
              <Link to="/"> Home </Link>
            </li>
            /
            <li className="active">
              <Link to=""> {props.heading} </Link>
            </li>
          </ul>
        </div>
      </div>
    </section>
  );
}

export default PageTitle;
