import React from 'react'

function SubscribeArea() {
  return (
    <section
      id="subscribe"
      className="background-colored text-center p-t-80 p-b-30"
    >
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-5">
            <div className="widget widget-newsletter">
              <form
                id="subscribe-form"
                className="widget-subscribe-form"
                action="https://script.google.com/macros/s/AKfycbw8FUmeqj_0-IRiYSEa0Hq23_90pJdqMYwrRKkNt63ryXwsITkfbMujp2Lcy-WPQWhy/exec"
                role="form"
                method="post"
              >
                <h3 className="text-light">Subscribe to our Newsletter</h3>
                <div className="input-group mb-0">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <i className="fa fa-paper-plane"></i>
                    </span>
                  </div>
                  <input
                    id="sub-email"
                    type="email"
                    required
                    name="Email"
                    className="form-control required email"
                    placeholder="Enter your Email"
                  />
                  <div className="input-group-append">
                    <button
                      type="submit"
                      id="widget-subscribe-submit-button"
                      className="btn btn-light"
                    >
                      Subscribe
                    </button>
                  </div>
                </div>
                <small id="response2" className="text-light">
                  Stay informed on our latest news!
                </small>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default SubscribeArea