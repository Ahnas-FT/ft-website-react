import React from "react";
import Address from "../Address/Address";
import ContactForm from "../ContactForm/ContactForm";
import SocialMediaItem from "../SocialMediaItem/SocialMediaItem";
import ContactBg from "./../../Images/contactBg.png";

function ContactArea() {
  return (
    <section
      id="section7"
      className="p-t-150 p-b-200"
      data-bg-parallax={ContactBg}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-12">
                <h2 className="m-b-10 text-black">Get in Touch with Us</h2>
                <p className="lead"></p>
              </div>

              <Address
                name="Freston Analytics UK Ltd"
                lineOne="200 Brook Drive"
                lineTwo="Green Park, Reading"
                lineThree="RG2 6UB - United Kingdom"
              />
              <Address
                name="Freston Analytics IN"
                lineOne="Thanveer Complex"
                lineTwo="West Nadakkavu"
                lineThree="Kerala, India"
              />

              <div className="col-lg-12 m-b-30">
                <h4 className="text-black">We are social</h4>

                <div className="social-icons social-icons-light social-icons-colored-hover">
                  <ul>
                    <SocialMediaItem
                      icon="fab fa-linkedin"
                      url="https://www.linkedin.com/company/freston-analytics-pvt-ltd"
                    />

                    <SocialMediaItem
                      icon="fab fa-youtube"
                      url="https://www.youtube.com"
                    />

                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">

            <ContactForm/>

          </div>
        </div>
      </div>
    </section>
  );
}

export default ContactArea;
