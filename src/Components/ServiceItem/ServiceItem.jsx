import React from "react";

import { Link } from "react-router-dom";


function ServiceItem(props) {
  return (
    <div className="col">
      <div className="text-box hover-effect text-dark">
        <Link to="/service">
          <span className="iconify" data-icon={props.icon}></span>

          <h3>{props.name}</h3>
          <p>{props.detail}.</p>
        </Link>
      </div>
    </div>
  );
}

export default ServiceItem;
