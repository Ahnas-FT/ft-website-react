import React from 'react'
import ServicePageAreaItem from '../ServicePageAreaItem/ServicePageAreaItem'

function ServicePageArea() {
  return (
    <section>
      <div className="container">
        <div className="heading-text heading-section text-center">
          <h2>SERVICES</h2>
        </div>
        <div className="row">
          <ServicePageAreaItem
            name="Digital Transformation"
            icon="cil:apps-settings"
            firstpara="Digital transformation has become inevitable for companies to
                thrive in the digital transformative age that we live it. If you
                are toying with new ideas to refresh your business or scaling
                successes, we will partner with you to develop a robust,
                adaptable digital transformation strategy."
            secondpara="As your digital transformation partner, we will stay with you
                every step of the way – from conception to development and
                beyond."
          />

          <ServicePageAreaItem
            name="Software Engineering"
            icon="eos-icons:software-outlined"
            firstpara="We create bespoke software solutions and deliver them to you via
                a project management process that best fits your project and
                business set up."
            secondpara="Depending on your requirement, we can scope out your needs and
                establish a fixed budget for your software development project
                or allocate a dedicated team or contract developers with skills
                aligned to your development needs. We know no two projects are
                the same. So, to ensure you get the right people for your
                project, our team will work with you to carefully review your
                specific requirements - and find expert developers who are
                precisely matched to your project."
          />

          <ServicePageAreaItem
            name="Cloud Migration"
            firstpara="We help you decide the right Cloud strategy for your business
                and determine the right operating model, roadmap and ecosystem
                partnerships complemented by our industry knowledge and
                technology insights. Our Cloud migration team will then
                collaborate with you to ensure a seamless migration and
                modernization to Cloud that is secure, agile and cost-effective."
            icon="bi:cloud-check"
          />

          <ServicePageAreaItem
            name="Data Analytics"
            icon="clarity:analytics-outline-badged"
            firstpara="We can help uncover detailed patterns in customer behaviour and
                use frontier technology to accelerate growth, deliver
                competitive advantage and remove uncertainties in your business
                decisions."
            secondpara="Combining a deep knowledge of statistics and software
                development with a good understanding of your requirements, our
                talented team will help you deliver impactful insights that
                generate growth."
          />

          
        </div>
      </div>
    </section>
  );
}



export default ServicePageArea
