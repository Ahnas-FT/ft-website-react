import React from "react";

import { Link } from "react-router-dom";

function ServicePageAreaItem(props) {
  return (
    <div className="col-lg-6" data-animate="fadeInUp" data-animate-delay="0">
      <div className="icon-box effect medium border small">
        <div className="icon">
          <Link to="">
            <span className="iconify" data-icon={props.icon}></span>
          </Link>
        </div>
        <h3>{props.name}</h3>
        <p>{props.firstpara}</p>
        <p>{props.secondpara}</p>
      </div>
    </div>
  );
}

export default ServicePageAreaItem;
