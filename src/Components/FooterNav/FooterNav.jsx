import React from "react";
import { Link } from "react-router-dom";

function FooterNav(props) {
  return (
    <li>
      <Link to={props.url}>{props.name}</Link>
    </li>
  );
}

export default FooterNav;
