import React from "react";
import { Link } from "react-router-dom";


function Banner() {
  return (
    <section
      id="home"
      className="fullscreen"
      data-bg-parallax="images/bannerbg.jpeg"
    >
      <div className="bg-overlay" data-style="6"></div>
      <div
        className="shape-divider"
        data-style="6"
        data-position="top"
        data-flip-vertical="true"
      ></div>
      <div className="shape-divider" data-style="6"></div>
      <div className="container">
        <div className="container-fullscreen">
          <div className="text-middle">
            <div className="heading-text text-light col-lg-12">
              <h2 className="font-weight-800">
                <span>
                  We are <br />
                  Freston Analytics, <br />a software technology company
                </span>
              </h2>
              <p>
                We take technology forward – helping organisations innovate,
                scale and transform with bespoke digital products and services.
              </p>
              <Link
                to="/service"
                className="btn btn-light btn-outline btn-rounded"
              >
                Read More
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Banner;
